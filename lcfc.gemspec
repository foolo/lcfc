Gem::Specification.new do |s|
  s.name        = 'lcfc'
  s.version     = '0.0.1'
  s.date        = '2015-01-31'
  s.summary     = "lcfc - linux configuration file collector"
  s.description = "Collect and synchronize your personal configuration files"
  s.authors     = ["Olof Andersson"]
  s.email       = 'olof.andersson@gmail.com'
  s.files       = ["lib/lcfc.rb"]
  s.executables = ["lcfc"]
  s.homepage    = 'https://bitbucket.org/foolo/lcfc'
  s.license     = 'MIT'
end
