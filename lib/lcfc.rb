#TODO "add" command	(i.e. "lcfc add my_new_untracked_config_file.cfg" (remember to create collection-files directory if needed))
#TODO add _abs or _rel to all paths
#TODO error if repopath points at non-existing track-list
#TODO add "do not track this file" to .repopath.cfg

require 'optparse'
require 'fileutils'

module Lcfc
class Handler

$APP_NAME = "lcfc"
$BINARY_NAME = "lcfc"
$COLLECTION_LIST_FILENAME = "collection.list"
$CONF_FILE_REL = "~/.repopath.cfg"
$CONF_FILE_ABS = File.expand_path $CONF_FILE_REL

class Item
	def initialize(relpath, repo)
		@relpath = relpath
		@repo = repo
	end
	def relpath
		@relpath
	end
	def trackedPathAbs
		abspath = File.expand_path @relpath
	end
	def absPathInRepo
		basename = File.basename @relpath
		File.join(@repo.abspath, "collection-files", basename)
	end
end

class Repo
	def initialize(relpath)
		@relpath = relpath
	end
	def relpath
		@relpath
	end
	def abspath
		File.expand_path @relpath
	end
	def getListFileAbsPath
		File.join(abspath, $COLLECTION_LIST_FILENAME)
	end
	def getListFileRelPath
		File.join(@relpath, $COLLECTION_LIST_FILENAME)
	end
	def emptyLine? s
		s = s.strip
		return ((s == "") or (s.start_with? "#"))
	end
	def getFileList
		lines = File.readlines(getListFileAbsPath).collect{|line| line.chomp}
		lines.reject!{|line| emptyLine? line}
		lines.collect do |line|
			itemRelpath = line
			Item.new(itemRelpath, self)
		end
	end

	def saveConf
		f = File.open $CONF_FILE_ABS, 'w'
		f.puts File.expand_path abspath
	end
end




## Init ##

def nicePath path
	if path == "."
		"current directory"
	else
		path
	end
end

def init newRepodir
	repo = Repo.new(newRepodir)
	if File.exists? repo.getListFileAbsPath
		#TODO if in use: show "already in use"
		puts "#{$COLLECTION_LIST_FILENAME} already exists in #{nicePath(repo.relpath)}"
		puts "Use --use-repo to activate it."
	else
		f = File.open repo.getListFileAbsPath, 'w'
		f.puts "# This file contains a list of files to be tracked by #{$APP_NAME}."
		f.puts "# Each line specifies the path of a tracked file."
		f.puts "#"
		f.puts "# Examples:"
		f.puts "#"
		f.puts "#	 ~/.bashrc"
		f.puts "#	 ~/.config/myfile"
		f.puts "#	 /some/path/myfile2"
		f.puts "#"
		f.puts "# However, it is not necessary to update this file manually."
		f.puts "# The easiest way is to use the --add function."
		puts "Created #{repo.getListFileRelPath}"
		puts "Now use --add to start tracking your personal files"
		repo.saveConf
	end
end


## Use repo ##

def userepo newRepodir
	repo = Repo.new(newRepodir)
	if File.exists? repo.getListFileAbsPath
		puts "Using repository: #{repo.abspath}"
		repo.saveConf
		puts "You can now --update or --add files."
	else
		puts "No track-list file (#{$COLLECTION_LIST_FILENAME}) found in #{repo.relpath}"
	end
end


## Update / Info ##

def getItemStatus item
	return :missingInRepo unless File.exists? item.absPathInRepo
	return :missing unless File.exists? item.trackedPathAbs
	return :existing unless isLinked item
	return :ok
end

def isLinked item
	linkTarget = `readlink --canonicalize --no-newline #{item.trackedPathAbs}`
	return linkTarget == item.absPathInRepo
end

def handleExisting item
	if @options[:nobackup]
		File.delete item.trackedPathAbs
		puts "Removed #{item.trackedPathAbs}"
	else
		timestamp = Time.now.to_i
		backupFilepath = "#{item.trackedPathAbs}.#{timestamp}.backup"
		puts "Renaming #{item.trackedPathAbs} to #{backupFilepath}"
		File.rename item.trackedPathAbs, backupFilepath
	end
end

def createLink item
	FileUtils.mkdir_p(File.dirname(item.trackedPathAbs))
	`ln --symbolic --force #{item.absPathInRepo} #{item.trackedPathAbs}`
	# Todo show error message on fail
end

def handleLinkage item
	return true if isLinked item
	if File.exists? item.trackedPathAbs
		handleExisting item
	end
	puts "Creating link: #{item.absPathInRepo} -> #{item.trackedPathAbs}"
	createLink item
end

def showItemStatus status, item, showPossibleActions
	case status
	when :missingInRepo
		puts "File #{item.absPathInRepo} missing in repository. Nothing to link to."	## todo red color
	when :missing
		puts "Status: Missing." # TODO bold style or similar
		puts "Run --update to create links" if showPossibleActions
	when :existing
		puts "Status: Existing, but not linked." # TODO bold style or similar
		puts "Running --update will backup the existing file, and replace it with a link to your repository." if showPossibleActions
	else
		puts "OK! (linked to: #{item.absPathInRepo})"	## todo green color
	end
end

## Update ##

def updateItem item
	return if item.relpath.strip.empty?
	status = getItemStatus item
	if status == :missingInRepo
		puts "File #{item.absPathInRepo} missing in repository. Nothing to link to."	## todo red color
		#TODO add possibility to copy existing file to current dir
	else
		status = getItemStatus item
		if [:missing, :existing].include? status
			handleLinkage item
		end
		showItemStatus getItemStatus(item), item, false
	end
end


## Main program
def run

@options = {}
parser = OptionParser.new do |opts|
	opts.on("--nobackup", "Do not backup existing target files") do |o|
		@options[:nobackup] = o
	end
	opts.on("--new DIR", "Create an empty collection") do |dir|
		dir = "." if dir.nil?
		@options[:dir] = dir
		@options[:action] = :init
	end
	opts.on("--use DIR", "Use an existing collection") do |dir|
		@options[:dir] = dir
		@options[:action] = :userepo
	end
	opts.on("--sync", "Update/add new links. Needed if the collection has changed upstream") do |b|
		@options[:action] = :update
	end
	opts.on("--list", "Show information about synchronized files") do |o|
		@options[:action] = :info
	end
end

begin
	parser.parse!(ARGV)
rescue OptionParser::ParseError
	puts parser.help
	exit
end
if ARGV.any?
	puts parser.help
	exit
end

def getRepoFromConf
	if File.exists? $CONF_FILE_ABS
		relpath = (File.read $CONF_FILE_ABS).strip
		return Repo.new(relpath)
	else
		puts "Lcfc is not initialized."
		puts "Run '#{$BINARY_NAME} --use DIR' to connect to an existing collection or run '#{$BINARY_NAME} --new DIR' to create a new"
		exit
	end
end

case @options[:action]

when :init
	init @options[:dir]
when :userepo
	userepo @options[:dir]

when :update
	repo = getRepoFromConf
	repo.getFileList.each do |item|
		puts item.relpath
		updateItem item
		puts
	end
when :info
	repo = getRepoFromConf
	# TODO improve info with linkage status etc
	puts "Repodir: "	+ repo.relpath
	#TODO handle when tracklist does not exist
	repo.getFileList.each do |item|
		puts item.relpath
		showItemStatus getItemStatus(item), item, true
		puts
	end

else
	puts "No action specified"
	puts parser.help
end

end # Run


end # Class
end # Module

