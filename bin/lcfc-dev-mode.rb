#!/usr/bin/env ruby

if ARGV.empty?
	puts "Usage: lcfc-dev.rb path/to/lcfc.rb"
	exit
end
lcfc_path = ARGV.shift
lcfc_path = File.expand_path(lcfc_path)
require lcfc_path
handler = Lcfc::Handler.new
handler.run

