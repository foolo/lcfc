## End user installation

	sudo gem install lcfc

## Install from source

	gem build lcfc.gemspec
	sudo gem install lcfc-<version>.gem

## Running without installing

	./bin/lcfc-dev-mode.rb lib/lcfc.rb

